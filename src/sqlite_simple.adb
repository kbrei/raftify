with Interfaces.C.Pointers;
with Ada.Containers.Indefinite_Vectors;
with Ada.Containers; use Ada.Containers;

package body Sqlite_Simple is

   function Open (Path :  String) return Sqlite_Handle is
      --  From SQLite3 docs
      Read_Write : constant Sqlite_Flag := 16#00000002#;
      Create : constant Sqlite_Flag := 16#00000004#;
      Create_And_Write : constant Sqlite_Flag := Read_Write or Create;

      function Sqlite_Open
        (File_Name : chars_ptr;
         Handle : in out Sqlite_Handle;
         Flags : Sqlite_Flag := Create_And_Write;
         Vfs : chars_ptr := Null_Ptr)
        return Sqlite_Error_Code
      with Import, Convention => C, Link_Name => "sqlite3_open_v2";

      Path_C : chars_ptr;
      Result : Sqlite_Error_Code;

      Handle : Sqlite_Handle := No_Handle;

   begin
      Path_C := New_String (Path);

      Result := Sqlite_Open
        (File_Name => Path_C,
         Handle => Handle);

      Free (Path_C);

      Throw_On_Bad_Exit_Code (Error => Result);

      return Handle;
   end Open;

   -------------------------------------
   -- Quick and dirty Sqlite Bindings --
   -------------------------------------

   procedure Exec
     (Handle : Sqlite_Handle;
      Sql : String)
   is
      function Sqlite_Exec
        (Handle : Sqlite_Handle;
         Sql : chars_ptr;
         Callback : System.Address := System.Null_Address;
         User_Data : System.Address := System.Null_Address;
         Error_Message : in out chars_ptr)
        return Sqlite_Error_Code
      with Import, Convention => C, Link_Name => "sqlite3_exec";

      --  TODO: this actually acts on void pointers, but this is easier
      procedure Sqlite_Free (Data : chars_ptr)
      with Import, Convention => C, Link_Name => "sqlite3_free";

      Sql_C : chars_ptr := New_String (Sql);
      Error_Message_C : chars_ptr := Null_Ptr;
      Error_Code : Sqlite_Error_Code;
   begin
      Error_Code := Sqlite_Exec
        (Handle => Handle,
         Sql => Sql_C,
         Error_Message => Error_Message_C);

      Free (Sql_C);

      if Error_Message_C /= Null_Ptr then
         declare
            Message : constant String := Value (Error_Message_C);
         begin
            Sqlite_Free (Error_Message_C);

            Throw_On_Bad_Exit_Code
              (Error => Error_Code,
               Message => Message);
         end;
      end if;

      --  It may be possible to get no error message AND
      --  a non-SqliteOk return code
      Throw_On_Bad_Exit_Code (Error_Code);
   end Exec;

   function Prepare
     (Handle : Sqlite_Handle; Sql : String)
     return Sqlite_Statement
   is
      Use_Full_String : constant int := -1;

      function Sqlite_Prepare
        (Handle : Sqlite_Handle;
         Sql : chars_ptr;
         Sql_Length : int := Use_Full_String;
         Statement : in out Sqlite_Statement;
         Unused_Sql : in out chars_ptr)
        return Sqlite_Error_Code
      with Import, Convention => C, Link_Name => "sqlite3_prepare_v2";

      Unused_Sql : chars_ptr;
      Sql_C : chars_ptr := New_String (Sql);
      Statement : Sqlite_Statement;
      Error_Code : Sqlite_Error_Code;
   begin
      Error_Code := Sqlite_Prepare
        (Handle => Handle,
         Sql => Sql_C,
         Statement => Statement,
         Unused_Sql => Unused_Sql);

      Free (Sql_C);

      Throw_On_Bad_Exit_Code (Error_Code);

      return Statement;
   end Prepare;

   procedure Bind_Blob
     (Statement : Sqlite_Statement;
      Index : int;
      Base_Address : System.Address;
      Length : int)
   is
      function Sqlite_Bind_Blob
        (Statement : Sqlite_Statement;
         Index : int;
         Base_Address : System.Address;
         Length : int;
         Free_Callback : System.Address := Sqlite_Static)
        return Sqlite_Error_Code
      with Import, Convention => C, Link_Name => "sqlite3_bind_blob";

      Error_Code : Sqlite_Error_Code;
   begin
      Error_Code := Sqlite_Bind_Blob
        (Statement => Statement,
         Index => Index,
         Base_Address => Base_Address,
         Length => Length);

      Throw_On_Bad_Exit_Code (Error_Code);
   end Bind_Blob;

   procedure Bind
     (Statement : Sqlite_Statement;
      Index : int;
      Value : String)
   is
      Up_To_Terminator : constant int := -1;

      function Sqlite_Bind_Text
        (Statement : Sqlite_Statement;
         Index : int;
         Text : chars_ptr;
         Length : int := Up_To_Terminator;
         Free_Callback : System.Address := Sqlite_Transient)
        return Sqlite_Error_Code
      with Import, Convention => C, Link_Name => "sqlite3_bind_text";

      Error_Code : Sqlite_Error_Code;
      Value_C : chars_ptr := New_String (Value);
   begin
      Error_Code := Sqlite_Bind_Text
        (Statement => Statement,
         Index => Index,
         Text => Value_C);

      Free (Value_C);

      Throw_On_Bad_Exit_Code (Error_Code);
   end Bind;

   procedure Bind
     (Statement : Sqlite_Statement;
      Index : int;
      Value : Integer)
   is
      function Sqlite_Bind_Int
        (Statement : Sqlite_Statement;
         Index : int;
         Value : int)
        return Sqlite_Error_Code
      with Import, Convention => C, Link_Name => "sqlite3_bind_int";

      Error_Code : Sqlite_Error_Code;
   begin
      Error_Code := Sqlite_Bind_Int
        (Statement => Statement,
         Index => Index,
         Value => int (Value));

      Throw_On_Bad_Exit_Code (Error_Code);
   end Bind;

   procedure Finalize (Statement : Sqlite_Statement) is
      function Sqlite_Finalize
        (Statement : Sqlite_Statement)
        return Sqlite_Error_Code
      with Convention => C, Import, Link_Name => "sqlite3_finalize";

      Error_Code : Sqlite_Error_Code;
   begin
      Error_Code := Sqlite_Finalize (Statement);

      Throw_On_Bad_Exit_Code (Error_Code);
   end Finalize;

   function Step (Statement : Sqlite_Statement) return Sqlite_Error_Code is
      function Sqlite_Step
        (Statement : Sqlite_Statement)
        return Sqlite_Error_Code
      with Convention => C, Import, Link_Name => "sqlite3_step";

      Error_Code : Sqlite_Error_Code;


      function Acceptable (Code : Sqlite_Error_Code) return Boolean is
      begin
         return
           Code = Sqlite_Ok or
           Code = Sqlite_Done or
           Code = Sqlite_Row;
      end Acceptable;
   begin
      Error_Code := Sqlite_Step (Statement);

      if not Acceptable (Error_Code) then
         Throw_On_Bad_Exit_Code (Error_Code);
      end if;

      return Error_Code;
   end Step;

   function Column_Int
     (Statement : Sqlite_Statement;
      Index : int)
     return int
   is
      function Sqlite_Column_Int
        (Statement : Sqlite_Statement; Index : int)
        return int
      with Convention => C, Import, Link_Name => "sqlite3_column_int";
   begin
      --  typecheck
      if Column_Type (Statement, Index) /= Sqlite_Integer then
         raise Sqlite_Error with
           "Column " & int'Image (Index) &
           "has the wrong type. Expected INTEGER.";
      end if;

      return Sqlite_Column_Int (Statement, Index);
   end Column_Int;

   function Column_Text
     (Statement : Sqlite_Statement;
      Index : int)
     return String
   is
      function Sqlite_Column_Text
        (Statement : Sqlite_Statement; Index : int)
        return chars_ptr
      with Convention => C, Import, Link_Name => "sqlite3_column_text";

      Field_Length : int;

      --  should be reclaimed by sqlite
      Text_Ptr : chars_ptr;
   begin
      --  typecheck
      if Column_Type (Statement, Index) /= Sqlite_Text then
         raise Sqlite_Error with
           "Column " & int'Image (Index) &
           "has the wrong type. Expected TEXT.";
      end if;

      Field_Length := Column_Length (Statement, Index);
      Text_Ptr := Sqlite_Column_Text (Statement, Index);

      return Null_Ignore_Value (Text_Ptr, size_t (Field_Length));
   end Column_Text;

   function Column_Bytes
     (Statement : Sqlite_Statement;
      Index : int)
     return Bytes
   is
      package Blobs is new Interfaces.C.Pointers
        (Index => Positive,
         Element => Byte,
         Element_Array => Bytes,
         Default_Terminator => 0);

      function Sqlite_Column_Blob
        (Statement : Sqlite_Statement; Index : int)
        return Blobs.Pointer
      with Convention => C, Import, Link_Name => "sqlite3_column_blob";

      Field_Length : int;

      --  should be reclaimed by sqlite
      Blob_Ptr : Blobs.Pointer :=
        Sqlite_Column_Blob (Statement, Index);
   begin
      --  typecheck
      if Column_Type (Statement, Index) /= Sqlite_Blob then
         raise Sqlite_Error with
           "Column " & int'Image (Index) &
           "has the wrong type. Expected BLOB.";
      end if;

      Field_Length := Column_Length (Statement, Index);
      Blob_Ptr := Sqlite_Column_Blob (Statement, Index);

      return Blobs.Value (Blob_Ptr, ptrdiff_t (Field_Length));
   end Column_Bytes;

   function Column_Length
     (Statement : Sqlite_Statement;
      Index : int)
     return int
   is
      function Sqlite_Column_Bytes
        (Statement : Sqlite_Statement; Index : int)
        return int
      with Convention => C, Import, Link_Name => "sqlite3_column_bytes";
   begin
      return Sqlite_Column_Bytes (Statement, Index);
   end Column_Length;

   function Column_Type
     (Statement : Sqlite_Statement;
      Index : int)
     return Sqlite_Type
   is
      function Sqlite_Column_Type
        (Statement : Sqlite_Statement; Index : int)
        return Sqlite_Type
      with Convention => C, Import, Link_Name => "sqlite3_column_type";
   begin
      return Sqlite_Column_Type (Statement, Index);
   end Column_Type;


   procedure Close (Handle : Sqlite_Handle)
   is
      function Sqlite_Close
        (Handle : Sqlite_Handle)
        return Sqlite_Error_Code
      with Convention => C, Import, Link_Name => "sqlite3_close";

      Error_Code : Sqlite_Error_Code;
   begin
      Error_Code := Sqlite_Close (Handle);

      Throw_On_Bad_Exit_Code (Error_Code);
   end Close;

   function Read_Rows
     (Statement : Sqlite_Statement)
     return Result_Collection
   is
      Results : Result_Collection := Empty;
      Current_Result : Sqlite_Error_Code;
   begin

      Over_Rows : loop
         Current_Result := Step (Statement);

         exit Over_Rows when Current_Result /= Sqlite_Row;

         Append (Results, Extract_From_Row (Statement));
      end loop Over_Rows;

      --  Throw when we get something
      --  that's not Sqlite_Done.
      --  This means something bad happend during iteration.
      if Current_Result /= Sqlite_Done then
         Throw_On_Bad_Exit_Code (Current_Result);
      end if;

      return Results;
   end Read_Rows;

   function Read_Single_Row
     (Statement : Sqlite_Statement)
     return Result_Type
   is
      package Results is new Ada.Containers.Indefinite_Vectors
        (Index_Type => Positive, Element_Type => Result_Type);

      procedure Append
        (Collection : in out Results.Vector;
         Value : Result_Type)
      is
      begin
         Results.Append (Collection, Value);
      end Append;

      function Read_All_Rows is new Read_Rows
        (Result_Type => Result_Type,
         Extract_From_Row => Extract_From_Row,
         Result_Collection => Results.Vector,
         Empty => Results.Empty_Vector,
         Append => Append);

      Result_Collection : Results.Vector;
   begin
      Result_Collection := Read_All_Rows (Statement);

      if Result_Collection.Length >= 1 then
         return Results.First_Element (Result_Collection);
      else
         --  TODO: we don't have the proper error code here!
         Throw_On_Bad_Exit_Code (Sqlite_Generic_Error);

         raise Program_Error with
           "Read_Single_Row: no rows from query. On_Error didn't throw.";
      end if;
   end Read_Single_Row;

   --  UTILS
   procedure Throw_On_Bad_Exit_Code
     (Error : Sqlite_Error_Code;
      Message : String := "";
      Throw_On_Ok : Boolean := False)
   is
      --  const char *sqlite3_errstr(int);
      function Sqlite_Errstr (Error : Sqlite_Error_Code) return chars_ptr
      with Convention => C, Import, Link_Name => "sqlite3_errstr";

      Error_Message_C : constant chars_ptr := Sqlite_Errstr (Error);
      Error_Message : constant String := Null_Ignore_Value (Error_Message_C);

      Details : constant String :=
        (if Message = ""
         then ""
         else " Details: " & Message);

      Full_Message : constant String :=
        "Error Code" & Sqlite_Error_Code'Image (Error) & ": " &
        "Sqlite Message: " & Error_Message & Details;
   begin
      --  We don't have to free Error_Message_C, Sqlite does it!

      --  No Error, nothing to raise
      if (Error = Sqlite_Ok) and not Throw_On_Ok then
         return;
      end if;

      --  throw the appropiate exception
      raise Sqlite_Error with Full_Message;
   end Throw_On_Bad_Exit_Code;

   procedure Exec (Handle : Sqlite_Handle; Statement : Sqlite_Statement)
   is
      pragma Unreferenced (Handle);

      Step_Result : Sqlite_Error_Code;
   begin
      Step_Result := Step (Statement);

      if Step_Result = Sqlite_Row then
         Throw_On_Bad_Exit_Code
           (Error => Sqlite_Row,
            Message => "Exec_Statement: Unexpected row result");

      elsif not (Step_Result = Sqlite_Done or Step_Result = Sqlite_Ok) then
         Throw_On_Bad_Exit_Code (Step_Result);
      end if;

   end Exec;

   function Null_Ignore_Value
     (Ptr : chars_ptr)
     return String is
   begin
      return (if Ptr = Null_Ptr
              then ""
              else Value (Ptr));
   end Null_Ignore_Value;

   function Null_Ignore_Value
     (Ptr : chars_ptr;
      Length : size_t)
     return String is
   begin
      return (if Ptr = Null_Ptr then "" else Value (Ptr, Length));
   end Null_Ignore_Value;

end Sqlite_Simple;
