with Sqlite_Simple; use Sqlite_Simple;

--  Implementation of the Raft.Log.Log interface
--  using Sqlite to store everything.
--  This wraps around the following schema:
--
--  CREATE TABLE log
--    (logIndex integer primary key not null,
--     term integer not null,
--     stmArgument blob);
--
--  CREATE TABLE vars
--    (currentTerm integer not null,
--     votedFor text,
--     lastApplied integer not null,
--     stmClear boolean not null);
--
--  Where the vars table always has exactly one row in it.

package Raft.Log.Sqlite is
   type Log is new Raft.Log.Log with private;

   ------------------------
   --  Interface members --
   ------------------------

   --  Open the database given by Path
   --  If it doesn't exist, it's created
   --  and the schema and initial values
   --  are inserted.
   overriding function Open (Path : String) return Log;

   --  Close the database
   overriding procedure Close (This : in out Log);

   --  All of the Read_... methods might
   --  throw a Raft.Log.Read_Error
   --  if they run against a corrupted sqlite
   --  database.

   --  Read a Raft.Log_Entry out of the
   --  corresponding table.
   --  This will throw a Raft.Log.Read_Error
   --  if no entry has been written previously
   --  to that Index.
   overriding function Read_Entry
     (This : in out Log;
      Index : Log_Index)
     return Log_Entry;

   --  All the Read_... functions below
   --  return the corresponding persitent variable
   --  from the vars table.

   --  If nothing has been written, they default as follows:
   --    Current_Term => 0
   --    Voted_For => ""
   --    Last_Applied => 1
   --    Stm_Clear => False

   overriding function Read_Current_Term
     (This : in out Log)
     return Term_Number;

   overriding function Read_Voted_For
     (This : in out Log)
     return Node_Name;

   overriding function Read_Last_Applied
     (This : in out Log)
     return Log_Index;

   overriding function Read_Stm_Clear
     (This : in out Log)
     return Boolean;

   --  Write a new log entry into the
   --  log table.
   --  This does not check for gaps and
   --  blindly overwrites existing rows.
   --
   --  USE WITH CAUTION
   overriding procedure Write_Entry
     (This : in out Log;
      Index : Log_Index;
      Term : Term_Number;
      Value : not null access constant Bytes);

   --  All of the Write_... methods below
   --  write the corresponding variable to
   --  the vars table.
   --  No checks are made, the old value
   --  will be overwritten.

   overriding procedure Write_Current_Term
     (This : in out Log;
      New_Term : Term_Number);

   overriding procedure Write_Voted_For
     (This : in out Log;
      New_Name : Node_Name);

   overriding procedure Write_Last_Applied
     (This : in out Log;
      New_Index : Log_Index);

   overriding procedure Write_Stm_Clear
     (This : in out Log;
      New_Stm_Clear : Boolean);

   --  Removes all entries from the log table
   --  with an logIndex >= Beginning_At
   --  No sanity checks are made.
   --
   --  USE WITH CAUTION
   overriding procedure Discard
     (This : in out Log;
      Beginning_At : Log_Index);
private
   type Operation_Kind is (Open, Close, Read, Write, Discard);

   type Log is new Raft.Log.Log with
      record
         Handle : Sqlite_Handle := No_Handle;
      end record;

   --  all reading and writing of persistent vars share the
   --  code via the generics below (Generic_Write, Generic_Read)

   generic
      Param_Name : String;
      type Column_Type (<>) is private;

      with procedure Bind_Column
        (Statement : Sqlite_Statement;
         Val : Column_Type);

   procedure Generic_Write
     (This : in out Log;
      New_Value : Column_Type)
   with Inline;

   generic
      Param_Name : String;
      type Column_Type (<>) is private;

      with function Extract
        (Statement : Sqlite_Statement)
        return Column_Type;

   function Generic_Read
     (This : in out Log)
     return Column_Type
   with Inline;

   procedure Build_Schema (This : in out Log);
end Raft.Log.Sqlite;
