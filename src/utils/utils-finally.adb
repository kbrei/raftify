
package body Utils.Finally is

   overriding procedure Finalize (This : in out Clean_Up)
   is
   begin
      This.Proc.all;
   end Finalize;

end Utils.Finally;
