with Ada.Finalization; use Ada.Finalization;

package Utils.Finally is

   type Clean_Up (Proc : not null access procedure)
     is new Limited_Controlled with null record;

   overriding procedure Finalize (This : in out Clean_Up);
end Utils.Finally;
