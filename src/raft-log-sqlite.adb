with Interfaces.C; use Interfaces.C;
with Ada.Directories;
with Ada.Exceptions; use Ada.Exceptions;
with Utils.Finally; use Utils.Finally;

package body Raft.Log.Sqlite is

   overriding function Open (Path :  String) return Log is
      New_Db : constant Boolean := not Ada.Directories.Exists (Path);

      This : Log;
   begin
      --  TODO: we also have to check if it's a valid path or not!
      if Path = "" then
         raise Raft.Log.Open_Error with
           "The sqlite log requires Path " &
           "to be a valid path to some (maybe non-existent) file";
      end if;

      This.Handle := Sqlite_Simple.Open (Path);

      if New_Db then
         Build_Schema (This);
      end if;

      return This;
   exception
      when Error : Sqlite_Error =>
         raise Raft.Log.Open_Error with Exception_Message (Error);
   end Open;

   procedure Build_Schema (This : in out Log) is
      Create_Log : constant String :=
        "create table log " &
        "(logIndex integer primary key not null," &
        " term integer not null, stmArgument blob);";

      Create_Vars : constant String :=
        "create table vars " &
        "(currentTerm integer not null," &
        " votedFor text," &
        " lastApplied integer not null," &
        " stmClear boolean not null);";

      Init_Vars : constant String :=
        "insert into vars values (0, """", 1, 0);";
   begin
      Exec (Handle => This.Handle, Sql => Create_Log);
      Exec (Handle => This.Handle, Sql => Create_Vars);
      Exec (Handle => This.Handle, Sql => Init_Vars);
   end Build_Schema;

   overriding procedure Close (This : in out Log)
   is
   begin
      Close (This.Handle);
   exception
      when Error : Sqlite_Error =>
         raise Raft.Log.Close_Error with Exception_Message (Error);
   end Close;


   overriding procedure Write_Entry
     (This : in out Log;
      Index : Log_Index;
      Term : Term_Number;
      Value : not null access constant Bytes)
   is
      Insert_Sql : constant String :=
        "insert into log (logIndex, term, stmArgument) values (?,?,?);";
      Insert : Sqlite_Statement := No_Statement;

      procedure Clean is
      begin
         Finalize (Insert);
      end Clean;

      Clean_Statement : Clean_Up (Clean'Access);
      pragma Unreferenced (Clean_Statement);
   begin
      Insert := Prepare (This.Handle, Insert_Sql);
      Bind (Insert, 1, Integer (Index));
      Bind (Insert, 2, Integer (Term));
      Bind_Blob (Insert, 3, Value.all'Address, Value.all'Length);

      Exec (This.Handle, Insert);

   exception
      when Error : Sqlite_Error =>
         raise Raft.Log.Write_Error with Exception_Message (Error);
   end Write_Entry;

   procedure Generic_Write (This : in out Log; New_Value : Column_Type) is
      Sql : constant String :=
        "update vars set " & Param_Name & "=? where rowid=1";

      Statement : Sqlite_Statement := No_Statement;

      procedure Clean is
      begin
         Finalize (Statement);
      end Clean;

      Clean_Statement : Clean_Up (Clean'Access);

      pragma Unreferenced (Clean_Statement);
   begin
      Statement := Prepare (This.Handle, Sql);

      Bind_Column (Statement, New_Value);

      Exec (This.Handle, Statement);
   exception
      when Error : Sqlite_Error =>
         raise Raft.Log.Write_Error with Exception_Message (Error);
   end Generic_Write;

   function Generic_Read
     (This : in out Log)
     return Column_Type
   is
      Select_Sql : constant String :=
        "select " & Param_Name & " from vars where rowId = 1;";

      Select_Stmt : Sqlite_Statement;

      function Extract_From_Row
        (Statement : Sqlite_Statement)
        return Column_Type
      is
      begin
         return Extract (Statement);
      end Extract_From_Row;

      function Read_Row is new Read_Single_Row
        (Result_Type => Column_Type,
         Extract_From_Row => Extract_From_Row);

      procedure Clean is
      begin
         Finalize (Select_Stmt);
      end Clean;

      Clean_Statement : Clean_Up (Clean'Access);
      pragma Unreferenced (Clean_Statement);
   begin
      Select_Stmt := Prepare (This.Handle, Select_Sql);

      return Read_Row (Select_Stmt);
   exception
      when Error : Sqlite_Error =>
         raise Raft.Log.Read_Error with Exception_Message (Error);
   end Generic_Read;

   overriding procedure Discard
     (This : in out Log;
      Beginning_At : Log_Index)
   is
      Delete_Sql : constant String :=
        "delete from log where logIndex >= ?;";

      Delete : Sqlite_Statement;

      procedure Clean is
      begin
         Finalize (Delete);
      end Clean;

      Clean_Statement : Clean_Up (Clean'Access);
      pragma Unreferenced (Clean_Statement);
   begin
      Delete := Prepare (This.Handle, Delete_Sql);
      Bind (Delete, 1, Integer (Beginning_At));

      Exec (This.Handle, Delete);
   exception
      when Error : Sqlite_Error =>
         raise Raft.Log.Discard_Error with Exception_Message (Error);
   end Discard;

   --  Boring boilerplate: Instantiations of Generic_Write

   overriding procedure Write_Current_Term
     (This : in out Log;
      New_Term : Term_Number)
   is
      procedure Bind_Current_Term (S : Sqlite_Statement; T : Term_Number)
      is
      begin
         Bind (S, 1, Integer (T));
      end Bind_Current_Term;

      procedure Do_Write is new Generic_Write
        (Param_Name => "currentTerm",
         Column_Type => Term_Number,
         Bind_Column => Bind_Current_Term);
   begin
      Do_Write (This, New_Term);
   end Write_Current_Term;

   overriding procedure Write_Voted_For
     (This : in out Log;
      New_Name : Node_Name)
   is

      procedure Bind_Voted_For (S : Sqlite_Statement; N : Node_Name) is
      begin
         Bind (S, 1, String (N));
      end Bind_Voted_For;

      procedure Do_Write is new Generic_Write
        (Param_Name => "votedFor",
         Column_Type => Node_Name,
         Bind_Column => Bind_Voted_For);
   begin
      Do_Write (This, New_Name);
   end Write_Voted_For;

   overriding procedure Write_Last_Applied
     (This : in out Log;
      New_Index : Log_Index)
   is
      procedure Bind_Last_Applied (S : Sqlite_Statement; I : Log_Index)
      is
      begin
         Bind (S, 1, Integer (I));
      end Bind_Last_Applied;

      procedure Do_Write is new Generic_Write
        (Param_Name => "lastApplied",
         Column_Type => Log_Index,
         Bind_Column => Bind_Last_Applied);
   begin
      Do_Write (This, New_Index);
   end Write_Last_Applied;

   overriding procedure Write_Stm_Clear
     (This : in out Log;
      New_Stm_Clear : Boolean)
   is
      procedure Bind_Stm_Clear (S : Sqlite_Statement; B : Boolean) is
      begin
         Bind (S, 1, (if B then 1 else 0));
      end Bind_Stm_Clear;

      procedure Do_Write is new Generic_Write
        (Param_Name => "stmClear",
         Column_Type => Boolean,
         Bind_Column => Bind_Stm_Clear);
   begin
      Do_Write (This, New_Stm_Clear);
   end Write_Stm_Clear;

   --  Boring read instantiations

   overriding function Read_Entry
     (This : in out Log;
      Index : Log_Index)
     return Log_Entry
   is
      Select_Sql : constant String :=
        "select * from log where logIndex = ?;";
      Select_Stmt : Sqlite_Statement;

      function Extract_Log_Entry
        (Statement : Sqlite_Statement)
        return Log_Entry
      is
         Index : constant Log_Index :=
           Log_Index (Column_Int (Statement, 0));

         Term : constant Term_Number :=
           Term_Number (Column_Int (Statement, 1));

         Value : constant Bytes :=
           Column_Bytes (Statement, 2);
      begin
         return (Value'Length, Index, Term, Value);
      end Extract_Log_Entry;

      function Read_Row is new Read_Single_Row
        (Result_Type => Log_Entry,
         Extract_From_Row => Extract_Log_Entry);

      procedure Clean is
      begin
         Finalize (Select_Stmt);
      end Clean;

      Clean_Statement : Clean_Up (Clean'Access);
      pragma Unreferenced (Clean_Statement);
   begin
      Select_Stmt := Prepare (This.Handle, Select_Sql);
      Bind (Select_Stmt, 1, Integer (Index));

      return Read_Row (Select_Stmt);
   exception
      when Error : Sqlite_Error =>
         raise Raft.Log.Read_Error with Exception_Message (Error);
   end Read_Entry;

   overriding function Read_Current_Term
     (This : in out Log)
     return Term_Number
   is
      function Extract_Term (Statement : Sqlite_Statement) return Term_Number
      is
         Term : constant Term_Number :=
           Term_Number (Column_Int (Statement, 0));
      begin
         return Term;
      end Extract_Term;

      function Do_Read is new Generic_Read
        (Param_Name => "currentTerm",
         Column_Type => Term_Number,
         Extract => Extract_Term);
   begin
      return Do_Read (This);
   end Read_Current_Term;

   overriding function Read_Voted_For
     (This : in out Log)
     return Node_Name
   is
      function Extract_Name
        (Statement : Sqlite_Statement)
        return Node_Name
      is
         Name : constant Node_Name := Node_Name (Column_Text (Statement, 0));
      begin
         return Name;
      end Extract_Name;

      function Do_Read is new Generic_Read
        (Param_Name => "votedFor",
         Column_Type => Node_Name,
         Extract => Extract_Name);
   begin
      return Do_Read (This);
   end Read_Voted_For;

   overriding function Read_Last_Applied
     (This : in out Log)
     return Log_Index
   is
      function Extract_Log_Index
        (Statement : Sqlite_Statement)
        return Log_Index
      is
         Index : constant Log_Index := Log_Index (Column_Int (Statement, 0));
      begin
         return Index;
      end Extract_Log_Index;

      function Do_Read is new Generic_Read
        (Param_Name => "lastApplied",
         Column_Type => Log_Index,
         Extract => Extract_Log_Index);
   begin
      return Do_Read (This);
   end Read_Last_Applied;

   overriding function Read_Stm_Clear
     (This : in out Log)
     return Boolean
   is
      function Extract_Boolean
        (Statement : Sqlite_Statement)
        return Boolean
      is
         Clear : constant Boolean :=
           (Column_Int (Statement, 0) >= 1);
      begin
         return Clear;
      end Extract_Boolean;

      function Do_Read is new Generic_Read
        (Param_Name => "stmClear",
         Column_Type => Boolean,
         Extract => Extract_Boolean);
   begin
      return Do_Read (This);
   end Read_Stm_Clear;
end Raft.Log.Sqlite;
