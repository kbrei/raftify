with Raft; use Raft;
with Raft.Log.Sqlite; use Raft.Log.Sqlite;
with Ada.Text_IO; use Ada.Text_IO;

function Raftify_Stm
  return Integer
is
   Log : Raft.Log.Log'Class := Raft.Log.Sqlite.Open ("unversiond/test.db");

   Original : aliased constant Bytes := (1, 2, 255, 4);
   Value : aliased constant Bytes := Original (2 .. 3);
begin
   Put_Line ("Init worked");
   Put_Line ("Open worked");
   Log.Write_Entry (2, 3, Value'Access);
   Put_Line ("Write worked");
   declare
      L_Entry : constant Log_Entry := Log.Read_Entry (2);
   begin
      Put_Line ("Read back: " & Term_Number'Image (L_Entry.Term));
      Put_Line ("Read back: " & Log_Index'Image (L_Entry.Index));
      Put_Line ("Read back: " & Byte'Image (L_Entry.Value (1)));
      Put_Line ("Read back: " & Byte'Image (L_Entry.Value (2)));
   end;
   return 0;
end Raftify_Stm;
