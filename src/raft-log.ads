package Raft.Log is
   type Log is interface;

   Open_Error : exception;
   function Open (Path : String := "") return Log is abstract;

   Close_Error : exception;
   procedure Close (This : in out Log) is abstract;

   Read_Error : exception;

   function Read_Entry
     (This : in out Log;
      Index : Log_Index)
     return Log_Entry is abstract;

   function Read_Current_Term
     (This : in out Log)
     return Term_Number is abstract;

   function Read_Voted_For
     (This : in out Log)
     return Node_Name is abstract;

   function Read_Last_Applied
     (This : in out Log)
     return Log_Index is abstract;

   function Read_Stm_Clear
     (This : in out Log)
     return Boolean is abstract;

   Write_Error : exception;

   procedure Write_Entry
     (This : in out Log;
      Index : Log_Index;
      Term : Term_Number;
      Value : not null access constant Bytes) is abstract;

   procedure Write_Current_Term
     (This : in out Log;
      New_Term : Term_Number) is abstract;

   procedure Write_Voted_For
     (This : in out Log;
      New_Name : Node_Name) is abstract;

   procedure Write_Last_Applied
     (This : in out Log;
      New_Index : Log_Index) is abstract;

   procedure Write_Stm_Clear
     (This : in out Log;
      New_Stm_Clear : Boolean) is abstract;

   Discard_Error : exception;

   procedure Discard
     (This : in out Log;
      Beginning_At : Log_Index) is abstract;
end Raft.Log;
