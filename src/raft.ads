with Interfaces;

package Raft is
   type Node_Name is new String;

   type Log_Index is new Positive;
   type Term_Number is new Natural;

   subtype Byte is Interfaces.Unsigned_8;
   type Bytes is array (Positive range <>) of aliased Byte;

   type Log_Entry (Value_Length : Positive) is record
      Index : Log_Index;
      Term : Term_Number;
      Value : Bytes (1 .. Value_Length);
   end record;
end Raft;
