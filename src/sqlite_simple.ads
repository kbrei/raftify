with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings; use Interfaces.C.Strings;
with System;
with System.Storage_Elements;

with Raft; use Raft;

package Sqlite_Simple is

   --  Handle to the currntly open database
   --  Obtained by Open,
   --  destroyed by Close.
   type Sqlite_Handle is new System.Address;

   function Open (Path : String) return Sqlite_Handle;

   procedure Close (Handle : Sqlite_Handle);

   --  A Handle that does nothing.
   --  Used as default value.
   No_Handle : constant Sqlite_Handle :=
     Sqlite_Handle (System.Null_Address);

   --  A prepared statement.
   --  Call Prepare to create it.
   --  Call Bind to fill in the ? parameters.
   --  Call Step or Exec_... to run it.
   --  Call Finalize to clean it up.
   type Sqlite_Statement is new System.Address;

   --  A statement doing nothing.
   No_Statement : constant Sqlite_Statement :=
     Sqlite_Statement (System.Null_Address);

   --  All procedure may throw that exception
   --  when they hit an irrecoverable sqlite error
   Sqlite_Error : exception;

   --  Definitons for all kinds of fancy integers
   --  used by sqlite.

   --  Represents return codes from sqlite3_* routines.
   type Sqlite_Error_Code is new int;

   --  Represents the different elementary types
   --  supported by sqlite.
   type Sqlite_Type is new int;
   Sqlite_Integer : constant Sqlite_Type := 1;
   Sqlite_Float : constant Sqlite_Type := 2;
   Sqlite_Text : constant Sqlite_Type := 3;
   Sqlite_Blob : constant Sqlite_Type := 4;
   Sqlite_Null : constant Sqlite_Type := 5;

   --  Used to represent flags need for the
   --  sqlite3_open_v2 function.
   type Sqlite_Flag is mod 2 ** int'Size;

   --  Some error codes we need later
   --  All others lead to an Sqlite_Error being thrown.
   Sqlite_Ok : constant Sqlite_Error_Code := 0;
   Sqlite_Generic_Error : constant Sqlite_Error_Code := 1;

   Sqlite_Row : constant Sqlite_Error_Code := 100;
   Sqlite_Done : constant Sqlite_Error_Code := 101;

   --  Special "callbacks" use by sqlite3_bind_* to indicate
   --  that sqlite must *not* copy the data (Sqlite_Static) or
   --  that sqlite *must* copy the data (Sqlite_Transient)
   --
   --  We never use a proper callback.
   Sqlite_Static : constant System.Address :=
     System.Storage_Elements.To_Address (0);

   --  -1, but we can't use negative literals,
   --  so we do 2's complement by hand
   Sqlite_Transient : constant System.Address :=
     System.Storage_Elements.To_Address ((not 1) + 1);

   --  When Error /= Sqlite_Ok then
   --  it throws an Sqlite_Error
   --  with a message obtained from sqlite3_errstr
   --  and the user provided message.
   --  When Throw_On_Ok = True, the it throw even
   --  whhen Error = Sqlite_Ok
   procedure Throw_On_Bad_Exit_Code
     (Error : Sqlite_Error_Code;
      Message : String := "";
      Throw_On_Ok : Boolean := False);

   --  Run a SQL string
   procedure Exec
     (Handle : Sqlite_Handle;
      Sql : String);

   --  Run a prepared statement.
   --  Returned rows will be ignored.
   procedure Exec
     (Handle : Sqlite_Handle;
      Statement : Sqlite_Statement);

   --  Compile a (possibly parameterized with ?) SQL
   --  String to a Statement.
   function Prepare
     (Handle : Sqlite_Handle;
      Sql : String)
     return Sqlite_Statement;

   --  Bind values to paramters of a prepared statement
   --  Index := 1, means bind parameter one
   procedure Bind
     (Statement : Sqlite_Statement;
      Index : int;
      Value : String);

   procedure Bind
     (Statement : Sqlite_Statement; Index : int; Value : Integer);

   --  Bind_Blob works like Bind,
   --  but it does not copy the value in sqlite.
   --  You have to make sure that Base_Address
   --  still points to a valid memory adress
   --  when you *run* this statement!
   --
   --  USE WITH CAUTION
   procedure Bind_Blob
     (Statement : Sqlite_Statement;
      Index : int;
      Base_Address : System.Address;
      Length : int);

   --  Cleanup a statement you ran.
   --  Call this to avoid memory leaks.
   procedure Finalize
     (Statement : Sqlite_Statement);

   --  Run a statement.
   --  If a statement returns no rows, then it finishes
   --  directly with Sqlite_Done;
   --  However, if the statement returns rows, the
   --  Step will return Sqlite_Row.
   --  You then have to get the data out of that row
   --  via the column functions.
   --  You have to keep calling step until it
   --  finishes with Sqlite_Done or an exception.
   function Step
     (Statement : Sqlite_Statement)
     return Sqlite_Error_Code;

   --  Column_*: Extract data out of a row
   function Column_Int
     (Statement : Sqlite_Statement;
      Index : int)
     return int;

   function Column_Text
     (Statement : Sqlite_Statement;
      Index : int)
     return String;

   function Column_Bytes
     (Statement : Sqlite_Statement;
      Index : int)
     return Bytes;

   function Column_Length
     (Statement : Sqlite_Statement;
      Index : int)
     return int;

   function Column_Type
     (Statement : Sqlite_Statement;
      Index : int)
     return Sqlite_Type;

   ------------------
   -- Reading rows --
   ------------------

   --  You can either read a single row from a statement (Read_Single_Row)
   --  Or you can collect the results of all Rows (Read_Rows)
   --  Neither of these two function finalize the statement.

   generic
      type Result_Type (<>) is private;

      --  abstract collection type
      type Result_Collection (<>) is private;
      Empty : Result_Collection;

      --  combine current value with the
      --  rows that came before it.
      with procedure Append
        (Collection : in out Result_Collection;
         Value : Result_Type);

      --  Extract the data you are interested in
      --  from a row.
      with function Extract_From_Row
        (Statement : Sqlite_Statement)
        return Result_Type;

   --  Read_Rows will append (via Append) all values extrated
   --  by Extract_From_Row to the Empty abstract collection
   function Read_Rows
     (Statement : Sqlite_Statement)
     return Result_Collection;

   --  Like read rows, but it only returns the first result,
   --  if any are available.
   --  This will throw an Sqlite_Error when rows are returned from the
   --  Statement.
   generic
      type Result_Type (<>) is private;

      with function Extract_From_Row
        (Statement : Sqlite_Statement)
        return Result_Type;

   function Read_Single_Row
     (Statement : Sqlite_Statement)
     return Result_Type;

   function Null_Ignore_Value
     (Ptr : chars_ptr)
     return String;

   function Null_Ignore_Value
     (Ptr : chars_ptr;
      Length : size_t)
     return String;

end Sqlite_Simple;
