with Ahven; use Ahven;
with Ada.Directories; use Ada.Directories;
with Raft; use Raft;
with Ada.Exceptions; use Ada.Exceptions;

pragma Elaborate_All (Ahven);

package body Regressions is
   Test_Db_Path : constant String := "unversioned/test.db";

   procedure Add
     (T : in out Test_Case'Class;
      Routine : Simple_Test_Routine_Access;
      Name    : String)
     renames Ahven.Framework.Add_Test_Routine
   with Unreferenced;

   procedure Add
     (T : in out Test_Case'Class;
      Routine : Object_Test_Routine_Access;
      Name : String)
     renames Ahven.Framework.Add_Test_Routine;

   overriding procedure Initialize (T : in out Test) is
   begin
      Set_Name (T, "Regressions");

      Add (T, Init_Clean'Access,
           "Log initializes to a clean state");

      Add (T, Read_Write_Log_Entry'Access,
           "Write an entry and read the same entry back");

      Add (T, Read_Write_Persitent_Vars'Access,
           "Write persitent vars and read the same values back");

      Add (T, Read_Non_Existing_Entry_Fails'Access,
           "Reading a non-existant entry fails");
   end Initialize;

   overriding procedure Set_Up (T : in out Test) is
   begin
      T.Log := Raft.Log.Sqlite.Open (Test_Db_Path);
   end Set_Up;

   overriding procedure Tear_Down (T : in out Test) is
   begin
      null;
      T.Log.Close;
      Delete_File (Test_Db_Path);
   end Tear_Down;

   procedure Assert_Eq is new Assert_Equal (Term_Number, Term_Number'Image);
   procedure Assert_Eq is new Assert_Equal (Log_Index, Log_Index'Image);
   procedure Assert_Eq is new Assert_Equal (Boolean, Boolean'Image);
   procedure Assert_Eq
     (Name : Node_Name;
      Expected : Node_Name;
      Var_Name : String)
   is
   begin
      Assert (Name = Expected,
              Var_Name & " (Expected: " & String (Expected)
                & "; Got: " & String (Name) & ")");
   end Assert_Eq;

   procedure Init_Clean (T : in out Test_Case'Class) is
      Log : Raft.Log.Sqlite.Log := Test (T).Log;

      Current_Term : constant Term_Number := Log.Read_Current_Term;
      Last_Applied : constant Log_Index := Log.Read_Last_Applied;
      Stm_Clear : constant Boolean := Log.Read_Stm_Clear;
      Voted_For : constant Node_Name := Log.Read_Voted_For;
   begin
      Assert_Eq (Current_Term, 0, "Current_Term");
      Assert_Eq (Last_Applied, 1, "Last_Applied");
      Assert_Eq (Stm_Clear, False, "Stm_Clear");
      Assert_Eq (Voted_For, "", "Voted_For");
   end Init_Clean;

   procedure Read_Write_Log_Entry (T : in out Test_Case'Class) is
      Log : Raft.Log.Sqlite.Log := Test (T).Log;
      Data : aliased constant Bytes := (1, 2, 3, 4, 5);
   begin
      Log.Write_Entry (1, 2, Data'Access);

      declare
         Read_Result : constant Log_Entry := Log.Read_Entry (1);
      begin
         Assert_Eq (Read_Result.Index, 1, "Index");
         Assert_Eq (Read_Result.Term, 2, "Term");
         Assert (Read_Result.Value = Data, "Value");
      end;
   end Read_Write_Log_Entry;

   procedure Read_Write_Persitent_Vars (T : in out Test_Case'Class) is
      Log : Raft.Log.Sqlite.Log := Test (T).Log;
   begin
      Log.Write_Current_Term (42);
      Assert_Eq (Log.Read_Current_Term, 42, "Term_Number");

      Log.Write_Last_Applied (43);
      Assert_Eq (Log.Read_Last_Applied, 43, "Last_Applied");

      Log.Write_Stm_Clear (False);
      Assert_Eq (Log.Read_Stm_Clear, False, "Stm_Clear");

      Log.Write_Voted_For ("Node 1");
      Assert_Eq (Log.Read_Voted_For, "Node 1", "Voted_For");
   end Read_Write_Persitent_Vars;

   procedure Read_Non_Existing_Entry_Fails (T : in out Test_Case'Class) is
      Log : Raft.Log.Sqlite.Log := Test (T).Log;
   begin

      declare
         Result : constant Log_Entry := Log.Read_Entry (500);
         pragma Unreferenced (Result);
      begin
         Fail ("No error occurred");
      end;

   exception
      --  we expect a read error
      when Raft.Log.Read_Error => null;

      --  every other kind of error is wrong
      when Error : others => Fail
        ("Unexpected exception type: " & Exception_Name (Error) &
         ": " & Exception_Information (Error));

   end Read_Non_Existing_Entry_Fails;
end Regressions;
