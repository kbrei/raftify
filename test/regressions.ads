with Ahven.Framework; use Ahven.Framework;
with Raft.Log.Sqlite; use Raft.Log.Sqlite;

pragma Elaborate_All (Ahven.Framework, Ahven);

package Regressions
is
   type Test is new Test_Case with private;
   overriding procedure Initialize (T : in out Test);

   overriding procedure Set_Up (T : in out Test);
   overriding procedure Tear_Down (T : in out Test);

private
   type Test is new Test_Case
      with record
        Log : Raft.Log.Sqlite.Log;
      end record;

   procedure Init_Clean (T : in out Test_Case'Class);
   procedure Read_Write_Log_Entry (T : in out Test_Case'Class);
   procedure Read_Write_Persitent_Vars (T : in out Test_Case'Class);
   procedure Read_Non_Existing_Entry_Fails (T : in out Test_Case'Class);
end Regressions;
