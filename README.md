# Goal of the project
Raftiy aims at implementing the [raft consensus algorithm](https://raft.github.io/) where the state machine being replicated is a simple script or program.
Said script will be replicated using raft.

All network communication, log storage and basically everything that isn't the state machine, will be done by raftify.

Raftify will provide a couple of binaries

- `raftify-stm` : runs the state machine as a raft node
- `raftify-send` : send a request/state machine input to the cluster
- `raftify-ctl` : create/add/remove node, leader step down

The request will be passed to the scripts either as arguments or via stdin.

# Further reading
- [Original Raft paper](http://ramcloud.stanford.edu/raft.pdf)
- [Raft dissertation](https://github.com/ongardie/dissertation/blob/master/online-trim.pdf?raw=true)

# Status
- Sqlite based log implementation works
- Zero MQ bindings are WIP

# WARNING
This whole project is in a very early development stage.

Only very few things work and everything is subject to change.